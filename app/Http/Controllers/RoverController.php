<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoverRequest;

class RoverController extends Controller
{
    /**
     * This controller will make the new positions based on given input.
     *
     * @param RoverRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setPosition(RoverRequest $request)
    {
        // Getting the inputs coming from request.
        $upperRight = $request->input('upperRight');
        $rovers = $request->input('rovers');
        $plateauCoordinates = explode(' ', $upperRight);
        $plateauXPosition = $plateauCoordinates[0];
        $plateauYPosition = $plateauCoordinates[1];

        // Create a key-value which shows what the next heading will be.
        $headingDecisionMaker = [
            'NL' => 'W',
            'NR' => 'E',
            'SL' => 'E',
            'SR' => 'W',
            'EL' => 'N',
            'ER' => 'S',
            'WL' => 'S',
            'WR' => 'N'
        ];

        $newPositions = [];

        foreach ($rovers as $rover) {
            $coordinates = explode(' ', $rover['initialPoint']);
            $xPosition = intval($coordinates[0]);
            $yPosition = intval($coordinates[1]);
            $heading = $coordinates[2];
            $commands = str_split($rover['commands']);

            foreach ($commands as $command) {
                switch ($command) {
                    case 'L':
                        $heading = $headingDecisionMaker[$heading.$command];
                        break;
                    case 'R':
                        $heading = $headingDecisionMaker[$heading.$command];
                        break;
                    case 'M':
                        $result = $this->moveForwardByHeading($heading, $yPosition, $xPosition);

                        // Check if the new x position is not out of bound, set the new position.
                        if ($result['xPosition'] <= $plateauXPosition) {
                            $xPosition = $result['xPosition'];
                        }

                        // Check if the new y position is not out of bound, set the new position.
                        if ($result['yPosition'] <= $plateauYPosition) {
                            $yPosition = $result['yPosition'];
                        }

                        break;
                }
            }
            array_push($newPositions, "$xPosition $yPosition $heading");
        }

        return response()->json([ 'newPositions' => $newPositions ], 200);
    }

    /**
     * Change the coordinates based on heading.
     *
     * @param $heading
     * @param $y
     * @param $x
     * @return array
     */
    public function moveForwardByHeading($heading, $y, $x) {
        switch ($heading) {
            case "N":
                $y++;
                break;
            case "S":
                $y--;
                break;
            case "E":
                $x++;
                break;
            case "W":
                $x--;
                break;
        }

        return [ 'xPosition' => $x, 'yPosition' => $y ];
    }
}
