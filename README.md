<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Rest API : 

#### Rovers new position
This API will return the new positions of rovers

- **method** : `POST`

- **body** : 

    - upperRight: `The coordinates related to upper-right of plateau like:
    "5 5"`
    
    - rovers: Array of rovers which has commands and initialPoint property like example below:
            
            [
                {
                    "initialPoint": "1 2 N",
                    "commands": "LMLMLMLMM"
                },
                {
                    "initialPoint": "3 3 E",
                    "commands": "MMRMMRMRRM"
                }
            ]
     
- **url**: `/api/set-position`
